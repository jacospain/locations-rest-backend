# locations-backend #

## Build & Run ##

```sh
$ cd -rest--backend
$ sbt
> jetty:start
> browse
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.
