package com.project2224.locations.repositories

import scalikejdbc._

case class DistrictRaceWriteInAccessEntity(id: Int, districtRaceId: Int, writeInAccessId: Int)

class DistrictRaceWriteInAccessRepository {
  def getDistrictRaceWriteInAccess(districtRaceId: Int): List[DistrictRaceWriteInAccessEntity] = DB readOnly {
    implicit session =>
      sql"""select id, district_race_id, write_in_access_id from district_race_write_in_access
            where district_race_id = ${districtRaceId}"""
        .map(rs => DistrictRaceWriteInAccessEntity(
          rs.int("id"),
          rs.int("district_race_id"),
          rs.int("write_in_access_id"))
        ).list.apply()
  }
}
