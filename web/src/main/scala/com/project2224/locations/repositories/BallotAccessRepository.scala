package com.project2224.locations.repositories

import scalikejdbc._

import java.sql.Date

case class BallotAccessEntity(id: Int, name: String, recognizedParties: Boolean, filingDeadline: Date, filingFee: Int, signatureRequirement: Int, comments: Option[String], extendedComments: Option[String], source: Option[String])

class BallotAccessRepository {
  def getDistrictRaceWriteInAccess(districtRaceId: Int): List[BallotAccessEntity] = DB readOnly {
    implicit session =>
      sql"""select
       ba.id as id,
       ba.name as name,
       ba.recognized_parties as recognized_parties,
       ba.filing_deadline as filing_deadline,
       ba.filing_fee as filing_fee,
       ba.signature_requirement as signature_requirement,
       ba.comments as comments,
       ba.extended_comments as extended_comments,
       ba.source as source
       from ballot_access as ba
       inner join district_race_ballot_access drba on drba.ballot_access_id = ba.id
       where drba.district_race_id = ${districtRaceId}"""
        .map(rs => BallotAccessEntity(
          rs.int("id"),
          rs.string("name"),
          rs.boolean("recognized_parties"),
          rs.date("filing_deadline"),
          rs.int("filing_fee"),
          rs.int("signature_requirement"),
          rs.getOpt[String]("comments"),
          rs.getOpt[String]("extended_comments"),
          rs.getOpt[String]("source")
        )
        ).list.apply()
  }
}
