package com.project2224.locations.repositories

import scalikejdbc._

import java.sql.Date

case class StateEntity(id: Int, code: String, name: String, comments: Option[String], extendedComments: Option[String])

class StateRepository {
  def getById(stateId: Int): Option[StateEntity] = DB readOnly {
    implicit session =>
      sql"""select id, code, name, comments, extended_comments
         from state where id = ${stateId}"""
        .map(rs => StateEntity(
          rs.int("id"),
          rs.string("code"),
          rs.string("name"),
          rs.getOpt[String]("comments"),
          rs.getOpt[String]("extended_comments")
        )
        ).single.apply()
  }
}
