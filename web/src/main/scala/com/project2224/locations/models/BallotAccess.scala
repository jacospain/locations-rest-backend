package com.project2224.locations.models

import java.util.Date

case class BallotAccess(
                          id: Int,
                          name: String,
                          recognizedParties: Boolean,
                          filingDeadline: Date,
                          filingFee: Int,
                          signatureRequirement: Int,
                          comments: Option[String],
                          extendedComments: Option[String],
                          source: Option[String]
                        )
