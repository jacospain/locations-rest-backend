package com.project2224.locations.models

case class State(
                id: Int,
                code: String,
                name: String,
                comments: Option[String],
                extendedComments: Option[String]
                )
