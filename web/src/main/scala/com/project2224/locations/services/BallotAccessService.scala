package com.project2224.locations.services

import com.project2224.locations.models.BallotAccess
import com.project2224.locations.repositories.{BallotAccessEntity, BallotAccessRepository}

class BallotAccessService(ballotAccessRepository: BallotAccessRepository) {
  def getDistrictRaceBallotAccess(districtRaceId: Int): List[BallotAccess] = {
    val entities = ballotAccessRepository.getDistrictRaceWriteInAccess(districtRaceId)
    entities.map(entity => modelFromEntity(entity))
  }

  def modelFromEntity(entity: BallotAccessEntity): BallotAccess = {
    BallotAccess(
      entity.id,
      entity.name,
      entity.recognizedParties,
      entity.filingDeadline,
      entity.filingFee,
      entity.signatureRequirement,
      entity.comments,
      entity.extendedComments,
      entity.source
    )
  }
}
