package com.project2224.locations.services

import com.project2224.locations.models.{BallotAccess, District, DistrictRace, WriteInAccess}
import com.project2224.locations.repositories.{DistrictRaceEntity, DistrictRaceRepository}

class DistrictRaceService(
                         districtRaceRepository: DistrictRaceRepository,
                         districtService: DistrictService,
                         ballotAccessService: BallotAccessService,
                         writeInAccessService: WriteInAccessService
                         ) {
  def getById(id: Int): DistrictRace = {
    val raceEntity = districtRaceRepository.getDistrictRaceByDistrictRaceId(id).get
    val district = districtService.getById(raceEntity.district_id)
    val ballotAccesses = ballotAccessService.getDistrictRaceBallotAccess(id)
    val writeInAccesses = writeInAccessService.getDistrictRaceWriteInAccess(id).get
    modelFromEntityAndRelations(raceEntity, district, ballotAccesses, writeInAccesses)
  }

  def getByStateCode(stateCode: String): List[DistrictRace] = {
    val raceEntities = districtRaceRepository.getDistrictRacesByStateCode(stateCode)
    raceEntities.map(districtRaceEntity => {
      val district = districtService.getById(districtRaceEntity.district_id)
      val ballotAccesses = ballotAccessService.getDistrictRaceBallotAccess(districtRaceEntity.id)
      val writeInAccesses = writeInAccessService.getDistrictRaceWriteInAccess(districtRaceEntity.id).get
      modelFromEntityAndRelations(districtRaceEntity, district, ballotAccesses, writeInAccesses)
    })
  }


  def modelFromEntityAndRelations(
                                   entity: DistrictRaceEntity,
                                   district: District,
                                   ballotAccesses: List[BallotAccess],
                                   writeInAccess: WriteInAccess
                                 ): DistrictRace = {
    DistrictRace(
      entity.id,
      district,
      new java.util.Date(entity.date.getTime),
      entity.special,
      ballotAccesses,
      writeInAccess
    )
  }
}

