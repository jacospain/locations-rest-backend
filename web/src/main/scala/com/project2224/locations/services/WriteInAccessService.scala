package com.project2224.locations.services

import com.project2224.locations.models.WriteInAccess
import com.project2224.locations.repositories.{WriteInAccessEntity, WriteInAccessRepository}

class WriteInAccessService(writeInAccessRepository: WriteInAccessRepository) {
  def getDistrictRaceWriteInAccess(districtId: Int): Option[WriteInAccess] = {
    val entity = writeInAccessRepository.getDistrictRaceWriteInAccess(districtId)
    entity.map(modelFromEntity)
  }

  def modelFromEntity(entity: WriteInAccessEntity): WriteInAccess = {
    WriteInAccess(
      entity.id,
      entity.name,
      entity.filingDeadline,
      entity.filingFee,
      entity.signatureRequirement,
      entity.comments,
      entity.extendedComments,
      entity.source
    )
  }
}
