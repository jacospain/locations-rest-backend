package com.project2224.locations.controllers

import org.json4s.{CustomSerializer, JNull, JString}

import java.time.format.DateTimeFormatter

object SimpleDateFormatter extends CustomSerializer[java.util.Date](format => (
{
case JString(d) => new java.util.Date() // SNJ TODO
case JNull => null
},
{
case x: java.util.Date =>
  val f = new java.text.SimpleDateFormat("yyyy-MM-dd")
  JString(f.format(x))
}
))


