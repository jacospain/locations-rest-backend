package com.project2224.locations.data

import com.zaxxer.hikari.{HikariConfig, HikariDataSource}

import java.sql.Connection

case class DatabaseConfig(jdbcUrl: String, username: String, password: String, maxLifetime: Long)

class LocationsDatabase(dbConfig: DatabaseConfig) {

  private val hikariConfig = new HikariConfig()
  hikariConfig.setJdbcUrl(dbConfig.jdbcUrl)
  hikariConfig.setUsername(dbConfig.username)
  hikariConfig.setPassword(dbConfig.password)
  hikariConfig.setMaxLifetime(dbConfig.maxLifetime)

  val hikariDataSource = new HikariDataSource(hikariConfig)

  def getConnection: Connection = hikariDataSource.getConnection()

}
