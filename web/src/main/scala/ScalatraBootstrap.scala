import com.project2224.locations.controllers.{DistrictController, HealthController}
import com.project2224.locations.data.{DatabaseConfig, LiquibaseRunner, LocationsDatabase}
import com.project2224.locations.repositories.{BallotAccessRepository, DistrictRaceRepository, DistrictRepository, StateRepository, WriteInAccessRepository}
import com.project2224.locations.services.{BallotAccessService, DistrictRaceService, DistrictService, StateService, WriteInAccessService}
import org.scalatra._
import scalikejdbc.{ConnectionPool, DataSourceConnectionPool}

import javax.servlet.ServletContext

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {
    val locationsDatabase = new LocationsDatabase(DatabaseConfig("jdbc:postgresql://localhost:5432/locations", "application_user", "", 60 * 10 * 1000))

    new LiquibaseRunner(locationsDatabase.getConnection).run()
    ConnectionPool.singleton(new DataSourceConnectionPool(locationsDatabase.hikariDataSource))
    context.mount(new HealthController, "/api/locations/health")
    context.mount(
      new DistrictController(
        new DistrictRaceService(
          new DistrictRaceRepository(),
          new DistrictService(
            new DistrictRepository(),
            new StateService(
              new StateRepository()
            )),
          new BallotAccessService(
            new BallotAccessRepository()
          ),
          new WriteInAccessService(
            new WriteInAccessRepository()
          )
        )
      ),
      "/api/locations/districts")
  }
}